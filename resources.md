---
title: resources
---

1. 课程网址 [6.828](https://pdos.csail.mit.edu/6.828/2018/schedule.html)
2. 实验源码 [xv6-expansion](https://github.com/professordeng/xv6-expansion)，每一个大实验分配一个独立的分支，方便大家学习。
3. 官方文档 [book-rev11.pdf](https://github.com/professordeng/xv6-book/tree/gh-pages/document)
4. 中文版的 MIT xv6 文档 [xv6-chinese](https://github.com/ranxian/xv6-chinese)
5. XV6 分页和保护实现 [paging and protection](https://www.cs.virginia.edu/~cr4bd/4414/F2018/paging-and-protection.html)
6. XV6 的学习顺序 [学生自述](https://zhuanlan.zhihu.com/p/74028717)